
/*
	Name: Advanced Admin Menu - ACE edition
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: config.hpp
	Description: Main config file for the mod, this is an important file as it loads the other files.
*/

#include "cfgVehicles.hpp"
#include "defines.hpp"

class CfgPatches
{
	class EB_AAM
	{
		author = "Elliot Beckett";
		url="";
		units[] = {};
		weapons[] = {};
		version="v.0.1";
		requiredAddons[] = {"cba_main","ace_main","ace_common","ace_interact_menu"};
	};
};

class CfgFunctions
{
	class EB_AAM_fnc
	{
		tag = "EB_AAM";
		class core
		{	
			file = "\EB_AAM\fnc\core";
			//Core admin check
			class AdminCheck {postInit = 1;};
			class Teleport_Messages{postInit = 1;};
			class AAM_Start {postInit = 1;};

			//Core functions
			class Set_Admin;
			class Set_Mission_Maker;
			class Diag_Message;
		};
		class monitoring_tools
		{
			file = "\EB_AAM\fnc\monitoring_tools";
			//Monitoring Tools
			class Monitor_On{};
			class Monitor_Off{};
			class ServerPerf{};
		};

		class ai_tools
		{
		
			file = "\EB_AAM\fnc\ai_tools";
			//AI Tools
			class Clear_Zeus_Cache{};
			class Delete_Opfor_AI{};
			class Delete_Blufor_AI{};
			class Delete_Indep_AI{};
			class Delete_Civ_AI{};
			class Delete_All_AI{};
			class Delete_Dead{};
		};

		class spawn_tools
		{
			file = "\EB_AAM\fnc\spawn_tools";
			//Spawn Tools
			class Spawn_ACE_Arsenal_Full{};
			class Spawn_ACE_Arsenal_Filtered{};
			class Delete_Arsenal_Box{};
		};
		class teleport_tools
		{

			file = "\EB_AAM\fnc\teleport_tools";
			//Teleport Tools
			class Teleport_Admin_To_Base{};
			class Teleport_Players_To_Admin{};
			class Teleport_Players_To_Base{};
		};

		/*
		class weather_tools
		{
			file = "EB_AAM\fnc\weather_tools";
			//Weather Tools
			//todo
		};
		*/
	};
};


class CfgRemoteExec
{
	class Functions
	{
		mode = 2;	// allowed
		jip = 1;	// JIP allowed

		//AI Tools remoteExec
		class EB_AAM_fnc_Clear_Zeus_Cache;
		class EB_AAM_fnc_Delete_Opfor_AI;
		class EB_AAM_fnc_Delete_Blufor_AI;
		class EB_AAM_fnc_Delete_Indep_AI;
		class EB_AAM_fnc_Delete_Civ_AI;
		class EB_AAM_fnc_Delete_All_AI;
		class EB_AAM_fnc_Delete_Dead;

		//Spawn Tools remoteExec

		class EB_AAM_fnc_Spawn_ACE_Arsenal_Full;
		class EB_AAM_fnc_Spawn_ACE_Arsenal_Filtered;
		class EB_AAM_fnc_Delete_Arsenal_Box;

		//Teleport Tools remoteExec

		class EB_AAM_fnc_Teleport_Admin_To_Base;
		class EB_AAM_fnc_Teleport_Players_To_Admin;
		class EB_AAM_fnc_Teleport_Players_To_Base;
	};
};