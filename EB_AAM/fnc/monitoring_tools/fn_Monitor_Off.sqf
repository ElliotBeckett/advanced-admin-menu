/*

	Name: Performance Monitoring - Turn Off
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Monitor_Off.sqf
	Description: Turns off the Monitoring Tool by setting the monitor to false and clears the hints. 

*/


for [{private _i = 0}, {_i < 2}, {_i = _i + 1}] do {
	hintSilent "";
	AAM_Zeus_Mon = false;
	hintSilent "";
};
