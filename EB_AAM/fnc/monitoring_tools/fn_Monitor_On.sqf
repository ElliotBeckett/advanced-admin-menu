/*

	Name: Performance Monitoring - Turn on
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Monitor_On.sqf
	Description: 
	Monitoring Tool - Count Units in mission and displays a message/hint to the screen. 
	The AI/Player totals update every 5 seconds, meaning that it updates with new players or AI spawned. 

*/

	AAM_Zeus_Mon = true;
	while {AAM_Zeus_Mon} do
	{
		private _OpforCount = east countSide (allUnits - allPlayers);
		private _BluforCount = west countSide (allUnits - allPlayers);
		private _IndepCount = independent countSide (allUnits - allPlayers);
		private _CivCount = civilian countSide (allUnits - allPlayers);
		private _PlayerCount = count allPlayers;
		private _TotalCount = _OpforCount + _BluforCount + _IndepCount + _CivCount + _PlayerCount;
		ServerFPS = (floor(diag_fps));
		publicVariable "ServerFPS";
		private _txt1 = parseText "<t color='#FF4242'>- Num of Opfor: ";
		private _txt2 = parseText "<t color='#5DADE2'>- Num of Blufor: ";
		private _txt3 = parseText "<t color='#67D213'>- Num of Independent: ";
		private _txt4 = parseText "<t color='#7B13D2'>- Num of Civilians: ";
		private _txt5 = parseText "<t color='#E3D30B'>- Num of Players: ";
		private _txt6 = parseText "<t color='#D00BE3'>- Total Count: ";
		private _txt7 = parseText "<t size='1.3' color='#00FF00'>- The Server can take more units ";
		private _txt8 = parseText "<t size='1.3' color='#FFA200'>- Slow down on spawning units ";
		private _txt9 = parseText "<t size='1.3' color='#FF0000'>- Stop Spawining Units or remove some  ";
		private _txt10 = parseText "<t color='#00FF00'>- Server fps: ";

		switch(true) do
		{
			case (_TotalCount <= 100):
			{
				ServerFPS = (floor(diag_fps));
				publicVariable "ServerFPS";
				private _structuredText = composeText [_txt1, str _OpforCount, lineBreak, lineBreak, _txt2, str _BluforCount,lineBreak, lineBreak,_txt3, str _IndepCount,lineBreak, lineBreak,_txt4, str _CivCount,lineBreak, lineBreak,_txt5, str _PlayerCount,lineBreak, lineBreak,_txt6, str _TotalCount,lineBreak,lineBreak,lineBreak,_txt7,lineBreak,lineBreak,lineBreak,_txt10, str ServerFPS];
				hintSilent _structuredText;
			};
			case (_TotalCount <= 149):
			{
				ServerFPS = (floor(diag_fps));
				publicVariable "ServerFPS";
				private _structuredText = composeText [_txt1, str _OpforCount, lineBreak, lineBreak, _txt2, str _BluforCount,lineBreak, lineBreak,_txt3, str _IndepCount,lineBreak, lineBreak,_txt4, str _CivCount,lineBreak, lineBreak,_txt5, str _PlayerCount,lineBreak, lineBreak,_txt6, str _TotalCount,lineBreak,lineBreak,lineBreak,_txt8,lineBreak,lineBreak,lineBreak,_txt10, str ServerFPS];
				hintSilent _structuredText;
			};
			case (_TotalCount >= 150):
			{
				ServerFPS = (floor(diag_fps));
				publicVariable "ServerFPS";
				private _structuredText = composeText [_txt1, str _OpforCount, lineBreak, lineBreak, _txt2, str _BluforCount,lineBreak, lineBreak,_txt3, str _IndepCount,lineBreak, lineBreak,_txt4, str _CivCount,lineBreak, lineBreak,_txt5, str _PlayerCount,lineBreak, lineBreak,_txt6, str _TotalCount,lineBreak,lineBreak,lineBreak,_txt9,lineBreak,lineBreak,lineBreak,_txt10, str ServerFPS];
				hintSilent _structuredText;
			};
			default
			{
			
			};
			
		};
		sleep 5;
	};
