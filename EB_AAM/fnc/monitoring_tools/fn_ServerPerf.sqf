AAM_OpforCount =
{
	east countSide allUnits;
};
publicVariable "OpforCount";

AAM_BluforCount =
{
	west countSide allUnits;
};
publicVariable "BluforCount";

AAM_IndepCount =
{
	independent countSide allUnits;
};
publicVariable "IndepCount";

AAM_CivCount =
{
	civilian countSide allUnits;
};
publicVariable "CivCount";

AAM_PlayerCount =
{
	count allPlayers;
};
publicVariable "PlayerCount";

AAM_TotalCount =
{
	AAM_OpforCount + AAM_BluforCount + AAM_IndepCount + AAM_CivCount + AAM_PlayerCount;
};
publicVariable "TotalCount";

AAM_Server_FPS =
{
	(floor(diag_fps));
};
publicVariable "Server_FPS";