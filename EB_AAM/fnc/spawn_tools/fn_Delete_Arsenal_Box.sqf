/*

	Name: Delete Arsenal Box
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Delete_Arsenal_Box.sqf
	Description:

	Deletes the arsenal box that the player is looking at

*/


private _debugMessage = ""; // Since there is a fail condition, we need to use a seperate style debug message

//Checks to see if the player is looking at the correct crate
if  ((cursorTarget) isKindOf "EB_AAM_Ammo_Crate") then  
{  	
	//Deletes the object the player is looking at
 	deleteVehicle cursorTarget; 
	//hint to confirm the box has been deleted. 
	hintSilent "Deleted Arsenal Box";
	
	_debugMessage = "Arsenal Box was successfully deleted";

}
else
//If there isn't an arsenal where the player is looking, display an error
{
	hintSilent "There are no valid arsenal boxes to delete where you are looking";
	_debugMessage = "Failed to delete Arsenal Box - No valid target was found";
	
};





///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Delete_Arsenal_Box"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 3) then {

	[fileName,_debugMessage] call EB_AAM_fnc_Diag_Message;
};