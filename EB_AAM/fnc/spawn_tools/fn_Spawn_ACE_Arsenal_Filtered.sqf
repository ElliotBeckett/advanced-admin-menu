/*

	Name: Spawn ACE Arsenal Filtered
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Spawn_ACE_Arsenal_Filtered.sqf
	Description:

	Spawns a filtered ACE Arsenal where the player is looking. 
	Filtering is dependent on what role the player is currently playing as. 
	Different Roles will see different options in the arsenal. 
	Everything can be customised, added to or removed. 

*/

/*

	Setting all of our local Vars to be private, so they can't be altered.
	It's going to look messy with a large amount of local vars (such as in this file), but it is needed. 
	It helps prevent players from injecting their own code into your variables. 
*/

private [
	//Base vars
	"_filteredBox",
	"_playerType",
	"_playerRole",

	//unit vars
	"_RiflemanDesert",
	"_RiflemanWoodland",

	//Gear Vars
	"_GeneralGear",
	"_DesertUniform",
	"_WoodlandUniform",
	"_infantryGuns",
	"_desertInfantryGuns",
	"_woodlandInfantryGuns",
	"_infantrySights",
	"_laserPointers",
	"_suppressors",

	//Assigned Gear Vars
	"_riflemanGearDesert",
	"_riflemanGearWoodland",
	"_defaultGear"

	// !! Remember, the last item in an array list does NOT have the Comma (,) at the end of it !! 
	
];


// Creating our Ammo box for the Arsenal to use. 
// Note, we are using the custom EB_AAM_Ammo_Crate so that we can also delete the arsenal box using the admin menu.  
_filteredBox = createVehicle ["EB_AAM_Ammo_Crate", screenToWorld [0.5,0.5], [], 0, "CAN_COLLIDE"]; 

// Returns the class name of the player to _playerType
_playerType = typeOf player; 


/* 
Grouping the Units together so we can assign gear to multiple units at once
You can set as many units as you want per group. Make sure to use the unit's classname and not description!

For example:

_unitGroup = "unit1Classname" + "unit2Classname" + "unit3Classname";

*/ 

_RiflemanDesert = "B_Soldier_F";
_RiflemanWoodland = "B_T_Soldier_F";

//Checking to see what role the player has taken, and then assign them to the correct gear
if (_playerType in _RiflemanDesert ) then {_playerRole = "riflemanDesert" };
if (_playerType in _RiflemanWoodland ) then {_playerRole = "riflemanWoodland" };


/// ~~~~ Gear Options ~~~~ \\\

/*

	This section defines what gear is available in the arsenal for players. 
	Feel free to add or remove any gear from this list. 
	This is just a basic set up to show what can be done and how to do it.

*/

/// General Gear, for everyone \\\
_GeneralGear = [

//Glasses 
"G_Tactical_Clear",
"G_Aviator",
"G_Combat",
"G_Bandanna_aviator",

//ACE Supplies
"ACE_fieldDressing",
"ACE_tourniquet",
"ACE_morphine",
"ACE_CableTie",
"ACE_EarPlugs",
"ACE_Flashlight_XL50",
"ACE_EntrenchingTool",

//Misc Items
"ItemGPS",
"ItemMap",
"ItemCompass",
"ItemWatch",
"NVGoggles",
"Binocular",
"FirstAidKit", // Added in case ACE medical is not enabled - Otherwise it will default to ACE med supplies
"Medikit", // Added in case ACE medical is not enabled - Otherwise it will default to ACE med supplies
"ToolKit",

//Grenades
"HandGrenade",
"MiniGrenade",
"SmokeShell",
"SmokeShellGreen",
"SmokeShellBlue",
"SmokeShellOrange",
"SmokeShellPurple",
"SmokeShellRed",
"SmokeShellYellow",
"B_IR_Grenade",
"ACE_Chemlight_White",
"Chemlight_yellow",
"Chemlight_red",
"Chemlight_green",
"Chemlight_blue"
];

//////////// Uniforms, Helmets, Vests & Backpacks \\\\\\\\\\\\
_DesertUniform = [

//Desert Uniforms
"U_B_CombatUniform_mcam",
"U_B_CombatUniform_mcam_tshirt",
"U_B_CombatUniform_mcam_vest",

//Desert Helmet
"H_HelmetB",
"H_HelmetB_black",
"H_HelmetB_desert",
"H_HelmetB_sand",

//Desert vests
"V_PlateCarrier1_rgr",
"V_PlateCarrier1_blk",

//Desert Backpacks

"B_AssaultPack_rgr",
"B_Carryall_mcamo",
"B_FieldPack_cbr",
"B_Kitbag_mcamo",
"B_TacticalPack_mcamo"

];

_WoodlandUniform = [

//Woodland Uniforms
"U_B_T_Soldier_F",
"U_B_T_Soldier_AR_F",
"U_B_T_Soldier_SL_F",

//Woodland Helmets
"H_HelmetB",
"H_HelmetB_black",
"H_HelmetB_grass",
"H_HelmetB_snakeskin",

//Woodland vests
"V_PlateCarrier1_tna_F",
"V_PlateCarrier1_blk",

//Woodland Backpacks
"B_AssaultPack_blk",
"B_Carryall_oli",
"B_FieldPack_blk",
"B_Kitbag_rgr",
"B_TacticalPack_oli"

];

/////////// Weapons & Accessories \\\\\\\\\\\\


//Generic Guns
_infantryGuns = [
"arifle_MX_Black_F",
"arifle_MXC_Black_F",
"30Rnd_65x39_caseless_black_mag",
"30Rnd_65x39_caseless_black_mag_Tracer",
"hgun_P07_F",
"16Rnd_9x21_Mag"
];

_desertInfantryGuns = [
"arifle_MX_F",
"arifle_MXC_F",
"30Rnd_65x39_caseless_mag",
"30Rnd_65x39_caseless_mag_Tracer"
];

_woodlandInfantryGuns = [
"arifle_MX_khk_F",
"arifle_MXC_khk_F",
"30Rnd_65x39_caseless_khaki_mag",
"30Rnd_65x39_caseless_khaki_mag_Tracer"
];

_infantrySights = [
"optic_Arco",
"optic_Arco_blk_F",
"optic_Holosight",
"optic_Holosight_blk_F",
"optic_Holosight_khk_F",
"optic_Hamr",
"optic_Hamr_khk_F"
];

_laserPointers = [
"acc_pointer_IR",
"acc_flashlight"
];

_suppressors = [
"muzzle_snds_H_snd_F",
"muzzle_snds_H_khk_F",
"muzzle_snds_H",
"muzzle_snds_L"
];

//Assigning each role the gear that they should have access too. 
_riflemanGearDesert = _GeneralGear + _DesertUniform  + _infantryGuns + _desertInfantryGuns  + _infantrySights + _laserPointers + _suppressors;
_riflemanGearWoodland = _GeneralGear + _WoodlandUniform   + _infantryGuns + _woodlandInfantryGuns + _infantrySights  + _laserPointers + _suppressors;


_defaultGear = _riflemanGearDesert + _riflemanGearWoodland;

/*
	The below switch statement cycles through the different player roles and sets the arsenal. 
	There is also a default case, in case a player is in a role that we haven't accounted for. 

	The Switch can be expanded to encompass as many cases as needed, but you can't define the same role twice.
*/

switch (_playerRole) do {
	///	Desert Units \\\
	case "riflemanDesert" : {
		[_filteredBox, _riflemanGearDesert, false ] call ace_arsenal_fnc_initBox;
		//[_filteredBox, player] call ace_arsenal_fnc_openBox;

	};


	/// Woodland Units \\\
	case "riflemanWoodland" : {
		[_filteredBox, _riflemanGearWoodland, false ] call ace_arsenal_fnc_initBox;
		//[_filteredBox, player] call ace_arsenal_fnc_openBox;

	};

	/// Default case, if all others fail \\\
	default {
		[_filteredBox, _defaultGear, false ] call ace_arsenal_fnc_initBox;
		//[_filteredBox, player] call ace_arsenal_fnc_openBox;
	};
};	



///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Spawn_ACE_Arsenal_Filtered"
#define debugMes "Filtered ACE Arsenal spawned"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 2) then {

	[fileName,debugMes] call EB_AAM_fnc_Diag_Message;
};
