/*

	Name: Spawn ACE Arsenal Full
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Spawn_ACE_Arsenal_Full.sqf
	Description:

	Spawns a full ACE Arsenal where the player is looking

*/

// This creates an ammo crate where the player is looking 
private _box = createVehicle ["EB_AAM_Ammo_Crate", screenToWorld [0.5,0.5], [], 0, "CAN_COLLIDE"];

//ACE Arsenal is then added to the box
[_box, true] call ace_arsenal_fnc_initBox;


///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Spawn_ACE_Arsenal_Full"
#define debugMes "Full ACE Arsenal spawned"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 2) then {

	[fileName,debugMes] call EB_AAM_fnc_Diag_Message;
};
