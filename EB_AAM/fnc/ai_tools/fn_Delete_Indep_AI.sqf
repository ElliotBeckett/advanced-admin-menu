/*
	Name: Delete Independent AI 
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Delete_Indep_AI
	Description: Deletes all Independent AI on the map (Including AI vehicles)

	
*/


{
	if (((side (effectiveCommander _x) == independent) && (!isPlayer effectiveCommander _x)) && !(_x isKindOf "UAV")) 
	then
	{
		deleteVehicle _x
	}
} forEach vehicles; // This deletes any Independent Controlled Vehicles

{
	if (((side _x == independent) && (!isPlayer _x)) && !((vehicle _x) isKindOf "UAV")) 
	then 
	{
		deleteVehicle _x
	}
} forEach allUnits; // This deletes any Independent AI


///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Delete_Indep_AI"
#define debugMes "All Independent AI have been deleted from the mission"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 2) then {

	[fileName,debugMes] call EB_AAM_fnc_Diag_Message;
};