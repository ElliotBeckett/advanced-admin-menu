
/*

	Name: Clear Zeus Cache
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Clear_Zeus_Cache.sqf
	Description:

	This clears the Zeus bug where too many units have been placed (as it counts dead units too)

*/

{deleteGroup _x} foreach allGroups;


///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Clear_Zeus_Cache"
#define debugMes "Zeus Cache has been cleared"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level == 3) then {

	[fileName,debugMes] call EB_AAM_fnc_Diag_Message;
};
