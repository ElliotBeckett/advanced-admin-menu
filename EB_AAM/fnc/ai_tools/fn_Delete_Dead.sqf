/*
	Name: Delete dead/empty units & vehicles
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Delete_Dead
	Description: Deletes all the Dead AI & vehicles on the map (except ones close to players)
*/

private _distance = 1500; //Distance from object to player, in Meters

{
	if (_x distance player > _distance ) then {
		if (_x isKindOf "Ship" || _x isKindOf "Air" || _x isKindOf "LandVehicle" || _x isKindOf "Wreck") then {
			_crew = nearestObjects [_x, ["Man"], 20];
			_crew = _crew + crew _x;
			deleteVehicle _x;
			{
				if (not alive _x) then {deleteVehicle _x};
			} forEach _crew;
		} else {
			deleteVehicle _x;
		};
	};
} forEach allDead;

///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Delete_Delete_Dead"
#define debugMes "All Dead AI / Empty Objects have been deleted from the mission"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 1) then {

	[fileName,debugMes] call EB_AAM_fnc_Diag_Message;
};