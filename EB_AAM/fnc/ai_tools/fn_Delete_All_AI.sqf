
/*
	Name: Delete all AI 
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Delete_All_AI
	Description: Deletes all the AI on the map (Including AI vehicles)


*/

{
	if (((side (effectiveCommander _x) == east) || (side (effectiveCommander _x) == west) || (side (effectiveCommander _x) == independent) || (side (effectiveCommander _x) == civilian) && (!isPlayer effectiveCommander _x)) && !(_x isKindOf "UAV") ) 
	then 
	{
		deleteVehicle _x
	}
} forEach vehicles; //This deletes any AI Controlled Vehicles

{ 
 if (((side _x == east) || (side _x == west) || (side _x == independent) || (side _x == civilian) && (!isPlayer _x)) && !((vehicle _x) isKindOf "UAV") )  
 then  
 { 
  deleteVehicle _x 
 } 
} forEach allUnits; //This deletes all AI soliders

///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Delete_All_AI"
#define debugMes "All AI have been deleted from the mission"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 1) then {

	[fileName,debugMes] call EB_AAM_fnc_Diag_Message;
};