/*

	Name: Teleport Players to Base
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Teleport_Players_To_Base.sqf
	Description:

	This teleports all players to the base/respawn marker

*/

private ["_baseMarker","_debugMessage"]; 

_baseMarker = "";   //Empty string to store respawn marker name into
_debugMessage = ""; // Since there is a fail condition, we need to use a seperate style debug message

switch(playerSide) do
{
	//West == blufor
	case west:
	{
	
		_baseMarker = "respawn_west";

	};

	//east == Opfor
	case east:
	{
		
		_baseMarker = "respawn_east";

	};

	//independent == independent or guerrila
	case independent:
	{
		 
		_baseMarker = "respawn_independent";

	};

	//Default option, if all others fail
	default
	{
		teleportPlayersToBaseMessage = failedTeleportMessage;
		publicVariable "teleportPlayersToBaseMessage";  
	};
	
};

// This is our teleport script

//If the marker position is [0,0,0] (basically, non exisitant), then display the error message
If ((getMarkerPos _baseMarker) isEqualTo [0,0,0])     
then    
{   
	teleportPlayersToBaseMessage = failedTeleportMessage;
	publicVariable "teleportPlayersToBaseMessage";   
	_debugMessage = "Unable to teleport Players to Base - Respawn Marker most likely missing or incorrectly named";
}   
else{   
    //If the marker exists, then teleport all players to the marker
 {    
  if(isPlayer _x) then   
  {   
   _x SetPos getMarkerPos _baseMarker;   
  }   
 } foreach allPlayers;   
 
	_debugMessage = "Successfully teleported all players to base";
};


///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Teleport_Admin_To_Base"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 3) then {

	[fileName,_debugMessage] call EB_AAM_fnc_Diag_Message;
};