/*

	Name: Teleport Admin to Base
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Teleport_Admin_To_Base.sqf
	Description:

	This teleports the Admin that used the function to their base/respawn marker

*/

private ["_baseMarker","_debugMessage"]; 

_baseMarker = "";   //Empty string to store respawn marker name into
_debugMessage = ""; // Since there is a fail condition, we need to use a seperate style debug message

switch(playerSide) do
{
	//West == blufor
	case west:
	{
	
		_baseMarker = "respawn_west";
		
	};

	//east == Opfor
	case east:
	{
		
		_baseMarker = "respawn_east";

	};

	//independent == independent or guerrila
	case independent:
	{
		 
		_baseMarker = "respawn_independent";

	};

	//Default option, if all others fail
	default
	{
		adminTeleportToBaseMessage = failedTeleportMessage;
		publicVariable "adminTeleportToBaseMessage";
	};
	
};
 

// This is our teleport script

//If the marker position is [0,0,0] (basically, non exisitant), then display the error message
If ((getMarkerPos _baseMarker) isEqualTo [0,0,0])     
then    
{   
 	adminTeleportToBaseMessage = failedTeleportMessage;
	publicVariable "adminTeleportToBaseMessage";
	_debugMessage = "Unable to complete Admin Teleport - Respawn Marker most likely missing or incorrectly named";
	

}   
else {  //If the marker exists, then teleport all players to the marker
  if(isPlayer player) then   
	{   
		player SetPos getMarkerPos _baseMarker;  
		_debugMessage = "Admin successfully teleported to base";
	};   
};

///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Teleport_Admin_To_Base"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 3) then {

	[fileName,_debugMessage] call EB_AAM_fnc_Diag_Message;
};