
/*

	Name: Teleport Players to Admin
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Teleport_Players_To_Admin.sqf
	Description:

	This teleports all players to the admin that called the function

*/

{
	//Checks if the entity is a player, and the player is alive, but not the player that triggered the function
	if((isPlayer _x && alive _x) && _x != player) then
	{
		//Sets the players to the position of the player that called the function
		_x SetPos getPos player;
	}
} foreach allPlayers; //Loops around for every player



///// DO NOT EDIT BELOW \\\\\

#define fileName "fn_Teleport_Players_To_Admin"
#define debugMes "All players have been teleported to Admin"
#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 3) then {

	[fileName,debugMes] call EB_AAM_fnc_Diag_Message;
};