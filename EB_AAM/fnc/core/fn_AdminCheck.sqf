
/*

	Name: Advanced Admin Menu - ACE edition
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_AdminCheck.sqf
	Description: This checks to see if the player is a valid admin, thus able to use the Admin Menu.


*/

// This is a Global variable for setting players to Admin or Mission Maker, please do not touch this (you will break everything). 
isPlayerAdmin = false;
publicVariable "isPlayerAdmin";
isPlayerMissionMaker = false;
publicVariable "isPlayerMissionMaker";

/*

	This is the list of UID's for the admins, this will make it so that people in this list will ALWAYS have FULL access to the admin menu. 
	They will not need to be a specific role in order to access the menu
	Only trusted players/owners should be added to this list. 

*/
adminUID = ["76561198076402389"];

//If the player is in the _adminUID array, set them to be an admin
if ((getplayerUID player in adminUID)) then
{
	isPlayerAdmin = true;
	publicVariable "isPlayerAdmin";
};

/*

	This is the list of UID's for the mission makers, this is for players that are running the mission or need temp access
	Players will also need to playing as a certain role (e.g. a NATO officer), in order to get access to the admin menu. 
	The role can be changed by changing the "B_officer_F" section. 
	For a list of Vanilla characters, see https://community.bistudio.com/wiki/Arma_3:_CfgVehicles_WEST

*/

missionMakerUID = [""];


//If the player is in the _missionMakerUID AND they are playing a NATO Officer role, set them to be an admin
if ((getplayerUID player in missionMakerUID) && ((typeOf player) in ["B_officer_F"])) then
{
	isPlayerMissionMaker = true;
	publicVariable "isPlayerMissionMaker";

};
	