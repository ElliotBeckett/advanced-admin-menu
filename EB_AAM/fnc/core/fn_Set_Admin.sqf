
/*
	Name: Set Admin
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Set_Admin.sqf
	Description:

	Allows Players to be manually set to Admin by using the Init box of the unit. 
	To do this, place a unit and in it's Init field type [_player] spawn EB_AAM_fnc_Set_Admin;

*/


if ((!(getplayerUID player in adminUID) || !(getplayerUID player in missionMakerUID))) then
{
	isPlayerAdmin = true;
	publicVariable "isPlayerAdmin";

}
else{

	hintSilent "You are alredy set as an Admin";
};

///// DO NOT EDIT BELOW \\\\\


#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 1) then {

	private _nameAdmin = name player;
	diag_log text format ["* AAM *: %1 has been set as an admin by calling the Set_Admin function. This is only temporary for this mission", _nameAdmin];
};