
/*

	Name: Diag Log Message
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Diag_Message.sqf
	Description:

	Custom Diag/RPT Logging message. 

	

	Syntax:
	[<SomeFile>,<MessageToLog>] call EB_AAM_fnc_Diag_Message;

	* SomeFile: STRING - Name of the File/Function called
	* MessageToLog: STRING - What message is to be displayed in the RPT message
	

*/


params [

	["_filename", "Default",[""]],
	["_debugMessage", "AAM Debug Default",[""]]
];

[_filename, _debugMessage] spawn {

	_filename = _this select 0;
	_debugMessage = _this select 1;

	private _nameAdmin = name player;
	
	diag_log text format ["* AAM *: %1 executed %2. The result was '%3'", _nameAdmin, _filename, _debugMessage];

};
