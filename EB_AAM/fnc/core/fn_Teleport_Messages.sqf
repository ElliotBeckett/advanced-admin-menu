/*

	Name: Teleport Messages
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Teleport_Messages.sqf
	Description:

	This compiles all the messages that are used for when players teleport. 
	It is in a seperate function to prevent some quirkey behaviour

*/



//Empty strings to store teleport messages into

adminTeleportToBaseMessage = ""; 
publicVariable "adminTeleportToBaseMessage";
teleportPlayersToBaseMessage = "";
publicVariable "teleportPlayersToBaseMessage";
failedTeleportMessage = "";
publicVariable "failedTeleportMessage";

//Compiling our failed to teleport message

_txt1 = parseText "Unable to complete player teleport ";
_txt2 = parseText "Please ensure that you are not hidden (if Zeus) or set to Civilian/Empty";
_txt3 = parseText "Also, ensure that the mission has a 'respawn_xxxx' marker placed on it";

failedTeleportMessage = composeText [_txt1, lineBreak, lineBreak, _txt2, lineBreak, lineBreak,_txt3, lineBreak];
publicVariable "failedTeleportMessage";

// Switch statement to check the side of the player to set the correct respawn marker, also sends the correct teleport message
switch(playerSide) do
{
	//West == blufor
	case west:
	{
		
		adminTeleportToBaseMessage = "Teleporting you to the Blufor Base"; 
		publicVariable "adminTeleportToBaseMessage";
		teleportPlayersToBaseMessage = "Teleporting all players to the Blufor Base";
		publicVariable "teleportPlayersToBaseMessage";
		
	};

	//east == Opfor
	case east:
	{
		 
		
		adminTeleportToBaseMessage = "Teleporting you to the Opfor Base"; 
		publicVariable "adminTeleportToBaseMessage";
		teleportPlayersToBaseMessage = "Teleporting all players to the Opfor Base";
		publicVariable "teleportPlayersToBaseMessage";


	};

	//independent == independent or guerrila
	case independent:
	{
		 
		adminTeleportToBaseMessage = "Teleporting you to the Independent Base"; 
		publicVariable "adminTeleportToBaseMessage";
		teleportPlayersToBaseMessage = "Teleporting all players to the Independent Base";
		publicVariable "teleportPlayersToBaseMessage";

	};

	//Default option, if all others fail
	default
	{
		adminTeleportToBaseMessage = failedTeleportMessage;
		publicVariable "adminTeleportToBaseMessage";
		teleportPlayersToBaseMessage = failedTeleportMessage;
		publicVariable "teleportPlayersToBaseMessage";

	};
	
};