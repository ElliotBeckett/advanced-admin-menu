/*
	Name: Set Admin
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: fn_Set_Mission_Maker.sqf
	Description:

	Allows Players to be manually set to a Mission Maker by using the Init box of the unit. 
	To do this, place a unit and in it's Init field type [_player] spawn EB_AAM_fnc_Set_Mission_Maker;

*/

if ((!(getplayerUID player in adminUID) || !(getplayerUID player in missionMakerUID))) then
{
	isPlayerMissionMaker = true;
	publicVariable "isPlayerMissionMaker";

}
else{

	hintSilent "You are alredy set as a Mission Maker";
};

///// DO NOT EDIT BELOW \\\\\


#include "..\..\defines.hpp"

//Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything

if (AAM_Debug_Level >= 1) then {

	private _nameAdmin = name player;
	diag_log text format ["* AAM *: %1 has been set as an Mission Maker by calling the Set_Mission_Maker function. This is only temporary for this mission", _nameAdmin];
};