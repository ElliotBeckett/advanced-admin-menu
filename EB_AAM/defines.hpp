//Defining our Debug Levels 0 = Off, 1 = Low level (only key items) 2 = Mid level (key items and a few others) 3 = Everything
#ifndef AAM_Debug_Level
#define AAM_Debug_Level 3
#endif

//Defining our fileName variable
#ifndef fileName
#define fileName "UNKNOWN"
#endif

//Define our Debug message
#ifndef debugMes
#define debugMes "Default Debug Message"
#endif
