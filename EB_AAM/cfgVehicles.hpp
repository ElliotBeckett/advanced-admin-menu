/*

	Name: Advanced Admin Menu - ACE edition
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: CfgVehicles.hpp
	Description: This adds all the interaction options to the players, allowing them to access the Admin menu. 

*/



class CfgVehicles {
	
    class Man;
    class CAManBase: Man {
        class ACE_SelfActions {

			//This is the main menu for all the admin tools, everything else will be a sub-menu of this
            class admin_Main {
                displayName = "<t color='#8060d2' size = '1.05'> Admin Menu";
                condition = "(isPlayerAdmin) || (isPlayerMissionMaker)"; // If a player is an admin or a mission maker, show this option
                exceptions[] = {};
                statement = "";
                icon = "\EB_AAM\AAM_Logo.paa";

				//This is the menu for the AI counter/Performance Monitoring pop ups
				class admin_Monitoring {
					displayName = "Performance Monitoring";
                	condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
                	exceptions[] = {};
                	statement = "";
                	icon = "\EB_AAM\icons\AAM_Mon_On.paa";

					//This turns on the Performance Monitoring pop up
					class admin_Monitoring_On {
					displayName = "Turn on Perf Mon";
                	condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
                	exceptions[] = {};
                	statement = "[_player] spawn EB_AAM_fnc_Monitor_On";
                	icon = "\EB_AAM\icons\AAM_Mon_On.paa";
					};

					//This turns off the Performance Monitoring pop up
					class admin_Monitoring_Off {
					displayName = "Turn off Perf Mon";
                	condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
                	exceptions[] = {};
                	statement = "[_player] spawn EB_AAM_fnc_Monitor_Off";
                	icon = "\EB_AAM\icons\AAM_Mon_Off.paa";
					};

				}; // End of Monitoring tools

				//This is the menu for all of our AI based controls. 
				class admin_AI_tools{
               	 displayName = "AI Tools";
               	 condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
               	 exceptions[] = {};
               	 statement = "";
				 icon = ""; //todo
				
					// This triggers the Zeus Cache clean on all clients
					class admin_AI_Clear_Zeus_Cache{
					displayName = "Clear Zeus Cache";
					condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
					exceptions[] = {};
					statement = "remoteExec ['EB_AAM_fnc_Clear_Zeus_Cache', 0, true]; hintSilent 'Zeus Cache cleared';";
					icon = ""; //todo
					};

					// This deletes all the Opfor AI on the map
					class admin_AI_Delete_Opfor_AI{
					displayName = "Delete All Opfor AI";
					condition = "isPlayerAdmin";
					exceptions[] = {};
					statement = "remoteExec ['EB_AAM_fnc_Delete_Opfor_AI', 0, true]; hintSilent 'All Opfor AI Deleted';";
					icon = ""; //todo
					};

					// This deletes all the Blufor AI on the map
					class admin_AI_Delete_Blufor_AI{
					displayName = "Delete All Blufor AI";
					condition = "isPlayerAdmin";
					exceptions[] = {};
					statement = "remoteExec ['EB_AAM_fnc_Delete_Blufor_AI', 0, true]; hintSilent 'All Blufor AI Deleted';";
					icon = ""; //todo
					};

					// This deletes all the Independent AI on the map
					class admin_AI_Delete_Indep_AI{
					displayName = "Delete All Independent AI";
					condition = "isPlayerAdmin";
					exceptions[] = {};
					statement = "remoteExec ['EB_AAM_fnc_Delete_Indep_AI', 0, true]; hintSilent 'All Independent AI Deleted';";
					icon = ""; //todo
					};

					// This deletes all the Civilian AI on the map
					class admin_AI_Delete_Civ_AI{
					displayName = "Delete All Civilian AI";
					condition = "isPlayerAdmin";
					exceptions[] = {};
					statement = "remoteExec ['EB_AAM_fnc_Delete_Civ_AI', 0, true]; hintSilent 'All Civilian AI Deleted';";
					icon = ""; //todo
					};

					// This deletes all the dead AI & Vehicles on the map
					class admin_AI_Delete_Dead{
					displayName = "<t color='#e6b800' size = '1.0'> Delete Dead Objects</t>";
					condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
					exceptions[] = {};
					statement = "remoteExec ['EB_AAM_fnc_Delete_Dead', 0, true]; hintSilent 'All Dead Objects Deleted';";
					icon = ""; //todo
					};

					// This deletes all the AI on the map
					class admin_AI_Delete_All_AI{
					displayName = "<t color='#FF4242' size = '1.1'> Delete All AI</t>";
					condition = "isPlayerAdmin";
					exceptions[] = {};
					statement = "remoteExec ['EB_AAM_fnc_Delete_All_AI', 0, true]; hintSilent 'All AI Deleted';";
					icon = ""; //todo
					};
					
				}; // End of AI tools

				class admin_Spawn_tools{
					displayName = "Spawning Tools";
					condition = "isPlayerAdmin";
					exceptions[] = {};
					statement = "";
					icon = ""; //todo

					class admin_Spawn_Arsenal_Full{
						displayName = "Spawn full ACE Arsenal";
						condition = "isPlayerAdmin";
						exceptions[] = {};
						statement = "remoteExec ['EB_AAM_fnc_Spawn_ACE_Arsenal_Full',0, true]; hintSilent 'Spawning Full Arsenal';";
						icon = ""; //todo
					};

					class admin_Spawn_Arsenal_Filtered{
						displayName = "Spawn Filtered ACE Arsenal";
						condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
						exceptions[] = {};
						statement = "remoteExec ['EB_AAM_fnc_Spawn_ACE_Arsenal_Filtered',0, true]; hintSilent 'Spawning Filtered Arsenal';";
						icon = ""; //todo
					};
					class admin_Delete_Arsenal_Box{
						displayName = "Delete Arsenal Box";
						condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
						exceptions[] = {};
						statement = "remoteExec ['EB_AAM_fnc_Delete_Arsenal_Box',0, true]; hintSilent 'Deleted Targeted Arsenal';";
						icon = ""; //todo
					};

				}; // End of spawn tools

				

				class admin_Teleport_tools{
					displayName = "Teleport Tools";
                	condition = "isPlayerAdmin";
                	exceptions[] = {};
                	statement = "";
                	icon = ""; //todo

					class admin_Teleport_To_Base{
						displayName = "Teleport me to Base";
                		condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
                		exceptions[] = {};
                		statement = "remoteExec ['EB_AAM_fnc_Teleport_Admin_To_Base',0, false]; hintSilent adminTeleportToBaseMessage;";
                		icon = ""; //todo
					};
	
					class admin_Teleport_Players_To_Admin{
						displayName = "Teleport all players to me";
                		condition = "isPlayerAdmin";
                		exceptions[] = {};
                		statement = "remoteExec ['EB_AAM_fnc_Teleport_Players_To_Admin',0, false]; hintSilent 'Teleported all players';";
                		icon = ""; //todo
					};

					class admin_Teleport_Players_To_Base{
						displayName = "Teleport all players to Base";
                		condition = "(isPlayerAdmin) || (isPlayerMissionMaker)";
                		exceptions[] = {};
                		statement = "remoteExec ['EB_AAM_fnc_Teleport_Players_To_Base',0, false]; hintSilent teleportPlayersToBaseMessage;";
                		icon = ""; //todo
					};
				}; //End of Teleport tools

				class admin_Weather_tools{
					displayName = "Weather Tools";
					condition = "isPlayerAdmin";
					exceptions[] = {};
					statement = hintSilent "This feature isn't implemented yet, sorry <3";
					icon = ""; //todo

				};// End of Weather Tools




			}; //End of Admin Main
		}; //End of ACE_SelfActions
	}; //End of Man

	/// DO NOT REMOVE 
	#include "cfgCrates.hpp"
}; //END of CfgVehicles