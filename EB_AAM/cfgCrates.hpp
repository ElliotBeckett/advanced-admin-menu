/*

	Name: cfgCrates
	Author: Elliot Beckett
	Date: 03/01/21
	File Name: cfgCrates.hpp
	Description:

	This just adds a custom (empty) crate that I can use for the Arsenal spawning script. 
	This will help prevent accidently deleting the wrong item 

*/

class B_supplyCrate_F;

class EB_AAM_Ammo_Crate : B_supplyCrate_F
{
	
	displayName = "AAM Virtual Ammo Box";
	scope = 1;
    scopeCurator = 1;
	scopeArsenal = 1;
	class TransportItems{};	
	class TransportMagazines{};	
	class TransportWeapons{};
	class TransportBackpacks{};
};